import { useState } from 'react'
import './App.css'
import { Routes, Route } from 'react-router-dom';
import Header from './components/Header';
import About from './components/About';
import Hero from './components/Hero';
import Card from './components/Card';
import Home_cards from './components/Home_cards';
import Footer from './components/Footer';

function App() {
  const [count, setCount] = useState(0)

  return (
    <>


      <div className="App">
        <Header />
        <Hero/>
        <Home_cards/>
        <Footer/>
        {/* <main>
        <Hero />
        <Download />
        <Features />
        <HowTorWorks />
      </main>
      <Footer /> */}
      </div>


      {/* <Routes>
        <Route path='/' element={<Header />}></Route>
        <Route path='/about' element={<About />}></Route>
      </Routes> */}
    </>
  )
}

export default App
