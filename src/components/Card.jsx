import React from 'react'
import './Card.css'

const Card = (props) => {
  
  return (
    <div>
      <div className="card__container">
        <div className="card__inside">
          <div className="card__row">
            <div className="card__pic_cont" style={{order: props.pic_order}}>
              <img src={props.image_link} alt=""/>
            </div>
            <div className="card__text_cont" style={{order: props.cont_order}}>
              <div className="text__inside_cont">
                <h4>{props.title}</h4>
                <p>{props.desc}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Card