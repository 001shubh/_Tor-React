import React from 'react'
import './Footer.css'

const Footer = () => {
    return (
        <div>
            <div className="main_footer">
                <div className="row_footer">
                    <div className="onion_pattern">
                        <img src="https://www.torproject.org/static/images/circle-pattern.svg?h=9a4040e4" alt="" />
                    </div>
                    <div className="download_section">
                        <div className="download_section_inside">
                            <h2>Download Tor Browser</h2>
                            <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
                            <a href="" className="footer_download__btn"> Download Tor Browser </a>
                        </div>
                    </div>
                </div>

                <div className="row_footer">
                    <div className="second_row">
                        <div className="our_mission_subscribe">
                            <h5>Our Mission: </h5>
                            <p className="mission">To advance human rights and freedoms by creating and deploying free and open source anonymity and privacy technologies, supporting their unrestricted availability and use, and furthering their scientific and popular understanding.</p>
                        </div>
                        <div className="footer_menu__social_icon">
                            <ul className="footer_menu__inside">
                                <li className="nav-item">
                                    <a href="" className="nav-link">Jobs</a>
                                </li>
                                <li className="nav-item">
                                    <a href="" className="nav-link">Blog</a>
                                </li>
                                <li className="nav-item">
                                    <a href="" className="nav-link">Contact</a>
                                </li>
                                <li className="nav-item">
                                    <a href="" className="nav-link">Press</a>
                                </li>
                                <li className="nav-item">
                                    <a href="" className="nav-link">Privchat</a>
                                </li>
                            </ul>
                            <a href="" className="f_donate_btn">
                                <span>Donate Now</span>
                            </a>
                        </div>
                    </div>
                </div>

                <div className="row_footer">
                    <div className="second_row">
                        <div className="our_mission_subscribe">
                            <p className="Subscribe">SUBSCRIBE TO OUR NEWSLETTER</p>
                            <p className="text">Get monthly updates and opportunities from the Tor Project:</p>
                            <p className="subscribe_btn">
                                <a href="">SIGN UP</a>
                            </p>
                        </div>
                        <div className="footer_menu__social_icon">
                            <div className="row_footer">
                                <h4><a href="">
                                    <i className="fas fa-band-aid">&copy;</i></a></h4>
                                <h4><a href="">
                                    <i className="fas fa-band-aid">&copy;</i></a></h4>
                                <h4><a href="">
                                    <i className="fas fa-band-aid">&copy;</i></a></h4>
                            </div>
                            <div className="row_footer">
                                <h4><a href="">
                                    <i className="fas fa-band-aid">&copy;</i></a></h4>
                                <h4><a href="">
                                    <i className="fas fa-band-aid">&copy;</i></a></h4>
                                <h4><a href="">
                                    <i className="fas fa-band-aid">&copy;</i></a></h4>
                            </div>
                            <div className="row_footer">
                                <h4><a href="">
                                    <i className="fas fa-band-aid">&copy;</i></a></h4>
                                <h4><a href="">
                                    <i className="fas fa-band-aid">&copy;</i></a></h4>
                                <h4><a href="">
                                    <i className="fas fa-band-aid">&copy;</i></a></h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row_footer">
                    <div className="second_row">
                        <div className="last_section">
                            <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href=""> FAQ.</a></p>
                        </div>

                        <div className="footer_menu__social_icon">
                            <div className="language_btn">
                                <button>English (en)</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
}

export default Footer