import React from 'react'
import './Hero.css'

const Hero = () => {
  return (
    <div>
      <div className="hero-sec">
        <div className="main-heading">
          Browse Privately.<br />Explore Freely.
        </div>
        <div className="sub-heading">Defend yourself against tracking and surveillance. Circumvent censorship.</div>
        <div className="hero__download__btn">
          <a href="">Download Tor Browser </a>
        </div>

      </div>
    </div>
  )
}

export default Hero