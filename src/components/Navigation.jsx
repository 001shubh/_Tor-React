import React from 'react'

const Navigation = () => {
  return (
    <>
        <nav className="header__nav">
            <ul className="header__list">
                <li className="header__item">
                    <Navlink exact to="/" className="header__nav-link">Home</Navlink>
                </li>
                <li className="header__item">
                    <Navlink to="/about" className="header__nav-link">About</Navlink>
                </li>
                <li className="header__item">
                    <Navlink to="/download" className="header__nav-link">Download</Navlink>
                </li>
                <li className="header__item">
                    <Navlink to="/documentation" className="header__nav-link">Documetation</Navlink>
                </li>
                <li className="header__item">
                    <Navlink to="/Community" className="header__nav-link">Community</Navlink>
                </li>
                <li className="header__item">
                    <Navlink to="/blog" className="header__nav-link">Blog</Navlink>
                </li>
            </ul>
        </nav>
    </>
  )
}

export default Navigation