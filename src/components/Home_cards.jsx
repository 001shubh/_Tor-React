import React from 'react'
import Card from './Card';
import './Home_cards.css'

const Home_cards = () => {
    return (
        <div>
            <div className="home__cards_cont">
                <div className="home__cards_inside">

                    <Card title="BLOCK TRACKERS" desc="Tor Browser isolates each website you visit so third-party trackers and ads can't follow you. Any cookies automatically clear when you're done browsing. So will your browsing history." pic_order="1" cont_order="2" image_link="https://www.torproject.org/static/images/home/svg/block-trackers.svg?h=70991bcb" />

                    <Card title="DEFEND AGAINST SURVEILLANCE" desc="Tor Browser prevents someone watching your connection from knowing what websites you visit. All anyone monitoring your browsing habits can see is that you're using Tor." pic_order="2" cont_order="1" image_link="https://www.torproject.org/static/images/home/svg/surveillance.svg?h=688a829c" />

                    <Card title="RESIST FINGERPRINTING" desc="Tor Browser aims to make all users look the same, making it difficult for you to be fingerprinted based on your browser and device information." pic_order="1" cont_order="2" image_link="https://www.torproject.org/static/images/home/svg/fingerprinting.svg?h=11fc8c97" />

                    <Card title="MULTI-LAYERED ENCRYPTION" desc="Your traffic is relayed and encrypted three times as it passes over the Tor network. The network is comprised of thousands of volunteer-run servers known as Tor relays." pic_order="2" cont_order="1" image_link="https://www.torproject.org/static/images/home/svg/encryption.svg?h=4b28f3dd" />

                    <Card title="BROWSE FREELY" desc="YWith Tor Browser, you are free to access sites your home network may have blocked." pic_order="1" cont_order="2" image_link="https://www.torproject.org/static/images/home/svg/browse-freely.svg?h=23b7d7d3" />
                </div>

                <div className="lower__hero">
                    <h6>ABOUT US</h6>
                    <p>We believe everyone should be able to explore the internet with privacy.
                        We are the Tor Project, a 501(c)(3) US nonprofit.
                        We advance human rights and defend your privacy online through free software and open networks. <a href="/about">Meet our team</a></p>
                </div>
            </div>

        </div>
    )
}

export default Home_cards