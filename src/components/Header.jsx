import React from 'react'
import './Header.css'
import './Navigation'

const Header = () => {
    return (
        <header className="header">
            <div className="header__inner">
                <nav className="navbar">
                    <a href="/" className="header__logo-link">
                        <img src="https://www.torproject.org/static/images/tor-logo@2x.png?h=16ad42bc" alt="Tor Project" />
                    </a>
                    <a href="" className="header__donate-btn">
                        <span className="header__btn">Donate Now</span>
                    </a>
                    <div className="header__nav">
                        <ul>
                            <li className="nav-item">
                                <a href="" className="nav-link">About</a>
                            </li>
                            <li className="nav-item">
                                <a href="" className="nav-link">Support</a>
                            </li>
                            <li className="nav-item">
                                <a href="" className="nav-link">Community</a>
                            </li>
                            <li className="nav-item">
                                <a href="" className="nav-link">Blog</a>
                            </li>
                            <li className="nav-item">
                                <a href="" className="nav-link">Donate</a>
                            </li>
                        </ul>
                    </div>
                    <div className="language">
                        <button>English (en)</button>
                    </div>
                    <div className="download__btn">
                        <a href="">Download Tor Browser </a>
                    </div>
                </nav>
            </div>
        </header>
    )
}

export default Header